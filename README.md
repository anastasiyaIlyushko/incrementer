# INCREMENTER

This library for Golang define and implement Incrementer interface.

[![go report card](https://goreportcard.com/report/bitbucket.org/anastasiyaIlyushko/incrementer "go report card")](https://goreportcard.com/report/bitbucket.org/anastasiyaIlyushko/incrementer)


## Overview

* Thread-safe
* Simple

## Example

```go
    inc := NewIncrementer()
	n := 10
	fmt.Println("set limit:", n) //init value
	inc.SetMaximumValue(n)
	fmt.Println("init value:", inc.GetNumber()) //init value

	inc.IncrementNumber()
	fmt.Println("once increment:", inc.GetNumber()) //once increment val = 1

	// increment to limit and cancel
	fmt.Println("increment to limit and cancel:")
	for i := 0; i < n; i++ {
		inc.IncrementNumber()
		fmt.Println(inc.GetNumber())
	}

	// start again
	inc.IncrementNumber()
	fmt.Println("start again:", inc.GetNumber())

	// Output:
	//set limit: 10
	//init value: 0
	//once increment: 1
	//increment to limit and cancel:
	//2
	//3
	//4
	//5
	//6
	//7
	//8
	//9
	//10
	//0
	//start again: 1
```

## License

Released under the [MIT License](https://bitbucket.org/anastasiyaIlyushko/incrementer/src/master/License)