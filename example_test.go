package incrementer_test

import (
	. "bitbucket.org/anastasiyaIlyushko/incrementer"
	"fmt"
)

func ExampleNewIncrementer() {
	inc := NewIncrementer()
	n := 10
	fmt.Println("set limit:", n) //init value
	inc.SetMaximumValue(n)
	fmt.Println("init value:", inc.GetNumber()) //init value

	inc.IncrementNumber()
	fmt.Println("once increment:", inc.GetNumber()) //once increment val = 1

	// increment to limit and cancel
	fmt.Println("increment to limit and cancel:")
	for i := 0; i < n; i++ {
		inc.IncrementNumber()
		fmt.Println(inc.GetNumber())
	}

	// start again
	inc.IncrementNumber()
	fmt.Println("start again:", inc.GetNumber())
	// Output:
	//set limit: 10
	//init value: 0
	//once increment: 1
	//increment to limit and cancel:
	//2
	//3
	//4
	//5
	//6
	//7
	//8
	//9
	//10
	//0
	//start again: 1
}
