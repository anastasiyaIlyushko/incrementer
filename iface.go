/*
Package incrementer define and implement Incrementer interface
*/
package incrementer

import (
	"sync"
)

//NewIncrementer return Incrementer
func NewIncrementer() Incrementer {
	return &incrementer{mu: &sync.Mutex{}}
}

//Incrementer is the interface implemented by types.
//Incrementer use for thread-safe increment int value to max value and cancel it after reaching the limit.
type Incrementer interface {

	//GetNumber return current value
	GetNumber() int

	//IncrementNumber increment value
	IncrementNumber()

	//SetMaximumValue set max value
	SetMaximumValue(maxVal int)
}
