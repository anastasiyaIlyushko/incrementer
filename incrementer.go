package incrementer

import (
	"sync"
)

const maxInt = int(^uint(0) >> 1)

//incrementer implement Incrementer interface
type incrementer struct {
	//stored value
	val int

	//max limit
	maxVal int

	//flag of limit setted
	isSetMaxVal bool

	//mutex for thread-safe call
	mu *sync.Mutex
}

//GetNumber return current value
func (i *incrementer) GetNumber() int {
	i.mu.Lock()
	defer i.mu.Unlock()

	return i.val
}

//IncrementNumber increment value
func (i *incrementer) IncrementNumber() {
	i.mu.Lock()
	defer i.mu.Unlock()

	maxVal := maxInt
	if i.isSetMaxVal {
		maxVal = i.maxVal
	}

	if i.val >= maxVal {
		i.val = 0
		return
	}
	i.val++
}

//SetMaximumValue set max value
func (i *incrementer) SetMaximumValue(maxVal int) {
	if maxVal < 0 {
		return
	}

	i.mu.Lock()
	defer i.mu.Unlock()

	if i.val >= maxVal {
		i.val = 0
	}

	i.maxVal = maxVal
	i.isSetMaxVal = true
}
