package incrementer_test

import (
	. "bitbucket.org/anastasiyaIlyushko/incrementer"
	"fmt"
	"github.com/magiconair/properties/assert"
	"math/rand"
	"sync"
	"testing"
	"time"
)

func TestMain(m *testing.M) {
	rand.Seed(time.Now().UnixNano())

	m.Run()
}

func TestInitialValue(t *testing.T) {
	inc := NewIncrementer()
	assert.Equal(t, inc.GetNumber(), 0, "initial value must be equal '0'")
}

func TestIncrementValueOnce(t *testing.T) {
	inc := NewIncrementer()
	inc.IncrementNumber()
	assert.Equal(t, inc.GetNumber(), 1, "value must be equal '1' after one call 'IncrementNumber()'")
}

func TestIncrementValueManyTimes(t *testing.T) {
	countCalls := getNonZeroRandom(1000)
	inc := NewIncrementer()
	for i := 0; i < countCalls; i++ {
		inc.IncrementNumber()
	}

	assert.Equal(t, inc.GetNumber(), countCalls,
		fmt.Sprintf("value must be equal '%v' after '%v' call 'IncrementNumber()'",
			countCalls,
			countCalls,
		),
	)
}

func TestIncrementValueThreadSafe(t *testing.T) {
	countCalls := getNonZeroRandom(1000)
	inc := NewIncrementer()

	var wg sync.WaitGroup
	for i := 0; i < countCalls; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			inc.IncrementNumber()
			inc.GetNumber()
		}()
	}

	wg.Wait()

	assert.Equal(t, inc.GetNumber(), countCalls,
		fmt.Sprintf("value must be equal '%v' after '%v' call 'IncrementNumber()'",
			countCalls,
			countCalls,
		),
	)
}

func TestSetMaxValueOnInitialState(t *testing.T) {
	inc := NewIncrementer()
	maxVal := getNonZeroRandom(100)
	inc.SetMaximumValue(maxVal)

	for i := 0; i <= maxVal; i++ {
		inc.IncrementNumber()
	}

	assert.Equal(t, inc.GetNumber(), 0,
		fmt.Sprintf("value must be '0' after '%v+1' calls  'IncrementNumber()'", maxVal))
}

func TestSetMaxValueLessCurrentValue(t *testing.T) {
	inc := NewIncrementer()
	n := 50
	countCalls := n + getNonZeroRandom(n)
	for i := 0; i < countCalls; i++ {
		inc.IncrementNumber()
	}

	maxVal := countCalls - getNonZeroRandom(n)
	inc.SetMaximumValue(maxVal)

	assert.Equal(t, inc.GetNumber(), 0,
		fmt.Sprintf("value must be '0' after setting maxVal (%v) less than current(%v)", maxVal, countCalls))
}

func TestSetMaxValueMoreCurrentValue(t *testing.T) {
	inc := NewIncrementer()
	n := 50
	countCalls := getNonZeroRandom(2 * n)
	for i := 0; i < countCalls; i++ {
		inc.IncrementNumber()
	}

	maxVal := countCalls + getNonZeroRandom(n)
	inc.SetMaximumValue(maxVal)

	assert.Equal(t, inc.GetNumber(), countCalls,
		fmt.Sprintf("value must be '%v' after setting maxVal (%v) more than current", countCalls, maxVal))
}

func TestSetMaxValueZero(t *testing.T) {
	inc := NewIncrementer()
	countCalls := getNonZeroRandom(100)
	maxVal := 0
	inc.SetMaximumValue(maxVal)

	for i := 0; i < countCalls; i++ {
		inc.IncrementNumber()
	}

	assert.Equal(t, inc.GetNumber(), 0, "value must be '0' after setting maxVal (0) always")
}

func TestSetMaxValueNegative(t *testing.T) {
	inc := NewIncrementer()
	initMaxVal := getNonZeroRandom(100)
	inc.SetMaximumValue(initMaxVal)
	maxVal := -getNonZeroRandom(100)
	inc.SetMaximumValue(maxVal)

	for i := 0; i <= initMaxVal; i++ {
		inc.IncrementNumber()
	}

	assert.Equal(t, inc.GetNumber(), 0,
		fmt.Sprintf("value must be '0' after initMaxVal(%v)+1 calls  'IncrementNumber()',"+
			" so negative max value ignore", initMaxVal))
}

func getNonZeroRandom(n int) int {
	random := rand.Intn(n)
	if random == 0 {
		return getNonZeroRandom(n)
	}

	return random
}
